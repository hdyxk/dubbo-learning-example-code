-- auto Generated on 2020-05-24 20:10:50 
-- DROP TABLE IF EXISTS `platform`; 
CREATE TABLE `platform`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'name',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`platform`';
