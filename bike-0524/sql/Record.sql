-- auto Generated on 2020-05-24 20:54:41 
-- DROP TABLE IF EXISTS `record`; 
CREATE TABLE `record`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `num` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'num',
    `detail` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'detail',
    `created_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'createdDate',
    `status` INT (11) NOT NULL DEFAULT -1 COMMENT 'status',
    `plat_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'platform',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`record`';
