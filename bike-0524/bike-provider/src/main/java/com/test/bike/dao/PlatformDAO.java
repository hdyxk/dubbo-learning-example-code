package com.test.bike.dao;

import com.test.bike.pojo.Platform;

import java.util.List;

public interface PlatformDAO {
    List<Platform> getPlatformList();
}
