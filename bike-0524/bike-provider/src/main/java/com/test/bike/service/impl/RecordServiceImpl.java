package com.test.bike.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.test.bike.dao.RecordDAO;
import com.test.bike.pojo.DubboResult;
import com.test.bike.pojo.Record;
import com.test.bike.pojo.RecordSearch;
import com.test.bike.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author 徒有琴
 */
@Service
public class RecordServiceImpl implements RecordService {

    @Autowired
    private RecordDAO recordDAO;

    @Override
    public PageInfo<Record> getRecordList(RecordSearch search, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(recordDAO.getRecordList(search), 5);
    }

    @Override
    public Record getRecordById(Integer id) {
        return recordDAO.getRecordById(id);
    }

    @Override
    public DubboResult addRecord(Record record) {
        try {
            int res = recordDAO.addRecord(record);
            return new DubboResult(res > 0, null);
        } catch (Exception e) {
            e.printStackTrace();
            return new DubboResult(false, e.getMessage());
        }
    }

    @Override
    public DubboResult updateRecord(Record record) {
        try {
            int res = recordDAO.updateRecord(record);
            return new DubboResult(res > 0, null);
        } catch (Exception e) {
            e.printStackTrace();
            return new DubboResult(false, e.getMessage());
        }
    }

    @Override
    public DubboResult deleteRecord(Integer id) {
        try {
            int res = recordDAO.deleteRecord(id);
            return new DubboResult(res > 0, null);
        } catch (Exception e) {
            e.printStackTrace();
            return new DubboResult(false, e.getMessage());
        }
    }
}
