package com.test.bike.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.test.bike.dao.PlatformDAO;
import com.test.bike.pojo.Platform;
import com.test.bike.service.PlatformService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author 徒有琴
 */
@Service
public class PlatformServiceImpl implements PlatformService {

    @Autowired
    private PlatformDAO platformDAO;


    @Override
    public List<Platform> getPlatformList() {
        return platformDAO.getPlatformList();
    }


}
