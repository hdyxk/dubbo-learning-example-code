package com.test.bike.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.test.bike.pojo.Platform;
import com.test.bike.pojo.Record;
import com.test.bike.pojo.RecordSearch;
import com.test.bike.service.PlatformService;
import com.test.bike.service.RecordService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;

/**
 * @author 徒有琴
 */
@Controller
public class BikeController {

    @Reference
    private PlatformService platformService;

    @Reference
    private RecordService recordService;

    @RequestMapping("list1.html")
    public String list(RecordSearch recordSearch, Integer pageNum, Model model) {
        if (pageNum == null || pageNum < 1) {
            pageNum = 1;
        }
        List<Platform> platforms = platformService.getPlatformList();
        model.addAttribute("platforms", platforms);
        PageInfo<Record> pageInfo = recordService.getRecordList(recordSearch, pageNum, 4);
        model.addAttribute("pageInfo", pageInfo);
        return "list";
    }

    @RequestMapping("edit.html")
    public String edit(Integer id, Model model) {
        if (id != null) {
            Record record = recordService.getRecordById(id);
            model.addAttribute("record", record);
        }
        List<Platform> platforms = platformService.getPlatformList();
        model.addAttribute("platforms", platforms);
        return "edit";
    }

    @RequestMapping("save.html")
    public String save(Record record) {
        if(record.getId()==null){
            record.setCreatedDate(new Date());
            recordService.addRecord(record);
        }else{
            recordService.updateRecord(record);
        }

        return "redirect:list.html";
    }

    @RequestMapping("delete.html")
    public String delete(Integer id){
        recordService.deleteRecord(id);
        return "redirect:list.html";
    }
}
