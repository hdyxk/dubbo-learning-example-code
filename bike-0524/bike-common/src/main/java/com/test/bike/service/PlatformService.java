package com.test.bike.service;

import com.test.bike.pojo.Platform;

import java.util.List;

public interface PlatformService {
    List<Platform> getPlatformList();
}
