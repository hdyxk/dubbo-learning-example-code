package com.test.dubbo.provider;

import com.alibaba.dubbo.config.annotation.Service;
import com.test.dubbo.service.HelloService;

/**
 * @author 徒有琴
 */
@Service
public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello(String name) {
        return "1hello " + name;
    }
}
