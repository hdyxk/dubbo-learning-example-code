package service;

/**
 * @author 徒有琴
 */
public interface HelloService {
    String sayHello();
}
