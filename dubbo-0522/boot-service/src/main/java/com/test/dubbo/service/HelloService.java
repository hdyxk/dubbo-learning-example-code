package com.test.dubbo.service;

/**
 * @author 徒有琴
 */
public interface HelloService {

    String sayHello(String name);
}
