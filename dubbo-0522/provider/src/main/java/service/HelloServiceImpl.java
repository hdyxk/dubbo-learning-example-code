package service;

/**
 * @author 徒有琴
 */

public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello() {
        return "Hello Dubbo";
    }
}
